$:.push File.expand_path("../lib", __FILE__)

require 'delayed_job_visual/version'

Gem::Specification.new do |s|
  s.name          = "delayed_job_visual"
  s.version       = DelayedJobVisual::VERSION
  s.authors       = ["Matt Young"]
  s.email         = ["mcyoung86@gmail.com"]

  s.summary       = %q{}
  s.description   = %q{}
  s.homepage      = ""
  s.license       = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_development_dependency "bundler", "~> 1.13"
  s.add_development_dependency "rake", "~> 10.0"
  s.add_development_dependency "rspec", "~> 3.0"
end
