DelayedJobVisual::Engine.routes.draw do
  resources :delayed_jobs do
    member do
      put :rerun
    end
  end

  root to: 'delayed_jobs#index'
end
