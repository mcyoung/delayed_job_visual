require_dependency 'delayed_job_visual/application_controller'

module DelayedJobVisual
  class DelayedJobsController < ApplicationController

    load_and_authorize_resource

    def index
      @delayed_jobs = @delayed_jobs.order("run_at ASC")
    end # index

    def show
      
    end # show

    def destroy
      @delayed_job.destroy
    end # destroy

    def rerun
      @delayed_job.update_attributes(attempts: 0, last_error: nil, locked_at: nil, failed_at: nil, locked_by: nil, run_at: Time.now)
      redirect_to delayed_jobs_path
    end # rerun

  end
end
